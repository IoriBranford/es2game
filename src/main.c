#include <stdio.h>
#include <glad/glad.h>
#include <tmx.h>
#include "render.h"
#include "render_tmx.h"
#include "free_camera_3d.h"
#include "game_spinning_tiles.h"
#include "SDL.h"

int main(int argc, char **args)
{
	SDL_Window *window;
	SDL_Init(SDL_INIT_EVERYTHING);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
	SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);

	int width = 640;
	int height = 480;
	window = SDL_CreateWindow(NULL,
			SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			width, height,
			SDL_WINDOW_FULLSCREEN_DESKTOP | SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);

	if (window == NULL) {
		printf("Failed to create window\n");
		SDL_Quit();
		return -1;
	}

	SDL_GetWindowSize(window, &width, &height);

	SDL_GL_CreateContext(window);
	if (!gladLoadGLES2Loader((GLADloadproc)SDL_GL_GetProcAddress)) {
		printf("Failed to initialize GLAD\n");
		SDL_Quit();
		return -1;
	}

	glViewport(0, 0, width, height);
	glDisable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	init_rendering();

	tmx_map *map = new_tmx_map("BKAM.tmx");
	if (!map) {
		SDL_Quit();
		return -1;
	}
	init_free_camera_3d();
	//init_spinning_tiles();

	Uint64 freq = SDL_GetPerformanceFrequency();
	Uint64 fixed_timestep = freq/60;
	float fixed_secs = (float)fixed_timestep/freq;
	Sint64 time_acc = 0;
	Uint64 time = SDL_GetPerformanceCounter();

	while (window) {
		Uint64 lftime = time;
		time = SDL_GetPerformanceCounter();
		Uint64 deltatime = time - lftime;
		time_acc += deltatime;

		SDL_Event e;
		while (SDL_PollEvent(&e)) {
			switch (e.type) {
			case SDL_QUIT:
				SDL_DestroyWindow(window);
				window = NULL;
				break;
			case SDL_KEYDOWN:
				switch (e.key.keysym.sym) {
				case SDLK_ESCAPE:
					SDL_DestroyWindow(window);
					window = NULL;
					break;
				}
				break;
			}
		}

		while (time_acc >= fixed_timestep) {
			update_free_camera_3d(fixed_secs);
			//update_spinning_tiles(fixed_secs);
			time_acc -= fixed_timestep;
		}

		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		//draw_spinning_tiles();
		draw_tmx_map(map);
		SDL_GL_SwapWindow(window);
	}

	//free_spinning_tiles();
	free_tmx_map(map);
	SDL_Quit();
	return 0;
}
