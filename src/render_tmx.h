#ifndef __RENDER_TMX_H__
#define __RENDER_TMX_H__

#include "linmath.h"

struct tmx_map;

void init_tmx_rendering();

tmx_map* new_tmx_map(const char *path);
void draw_tmx_map(tmx_map *map);
void free_tmx_map(tmx_map *map);

#endif
