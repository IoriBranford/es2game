#include "render.h"
#include "SDL.h"

static float yaw;
static float pitch;
static vec3 view_pos;
static mat4x4 view;

void init_free_camera_3d()
{
	int width, height;
	SDL_GetWindowSize(SDL_GL_GetCurrentWindow(), &width, &height);
	mat4x4 projection;
	mat4x4_perspective(projection, 1, (float)width/height, 1.f/1024, 1024);
	set_projection_matrix(projection);

	yaw = 0;
	pitch = 0;
	view_pos[0] = view_pos[1] = 0;
	view_pos[2] = -64;
	mat4x4_identity(view);

	SDL_SetRelativeMouseMode(1);
}

void update_free_camera_3d(float fixed_secs)
{
	int xrel, yrel;
	SDL_GetRelativeMouseState(&xrel, &yrel);
	yaw -= (float)M_PI*xrel/360;
	pitch -= (float)M_PI*yrel/360;

	vec3 view_forward = {0, 0, 1};
	vec3 view_right;
	vec3 view_up = {0, -1, 0};

	quat turn;
	quat_rotate(turn, yaw, view_up);
	quat_mul_vec3(view_forward, turn, view_forward);

	vec3_mul_cross(view_right, view_forward, view_up);
	quat_rotate(turn, pitch, view_right);
	quat_mul_vec3(view_forward, turn, view_forward);

	float speed = 128.f*fixed_secs;
	const Uint8* kbstate = SDL_GetKeyboardState(NULL);
	int key_f = kbstate[SDL_GetScancodeFromKey(SDLK_w)];
	int key_b = kbstate[SDL_GetScancodeFromKey(SDLK_s)];
	int key_l = kbstate[SDL_GetScancodeFromKey(SDLK_a)];
	int key_r = kbstate[SDL_GetScancodeFromKey(SDLK_d)];

	vec3 move_f;
	vec3_scale(move_f, view_forward, speed*(key_f - key_b));
	vec3_add(view_pos, view_pos, move_f);

	vec3 move_s;
	vec3_scale(move_s, view_right, speed*(key_r - key_l));
	vec3_add(view_pos, view_pos, move_s);

	vec3 view_target;
	vec3_add(view_target, view_pos, view_forward);
	mat4x4_look_at(view, view_pos, view_target, view_up);
	set_view_matrix(view);
}
