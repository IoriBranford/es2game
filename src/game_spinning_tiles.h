#ifndef __GAME_SPINNING_TILES_H__
#define __GAME_SPINNING_TILES_H__

void init_spinning_tiles();
void update_spinning_tiles(float fixed_secs);
void draw_spinning_tiles();
void free_spinning_tiles();

#endif
