#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glad/glad.h>
#include "render.h"

enum {
	POS_X, POS_Y, POS_Z,
	TEX_U, TEX_V
};

static int uTransform, uAttenuation;
static int aPos, aTexCoord;
static mat4x4 view_matrix;
static mat4x4 projection_matrix;
static mat4x4 view_projection_matrix;
static struct model null_model;

int check_compiled_shader(unsigned int shader, const char *type)
{
	int  success;
	char infoLog[512];
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

	if(!success) {
		glGetShaderInfoLog(shader, 512, NULL, infoLog);
		printf("ERROR::SHADER::%s::COMPILATION_FAILED\n", type);
		printf("%s\n", infoLog);
	}

	return success;
}

unsigned int make_shader_program(int vertlines, const char *vertsource[],
		int fraglines, const char *fragsource[])
{
	unsigned int vertexShader;
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, vertlines, vertsource, NULL);
	glCompileShader(vertexShader);

	unsigned int fragmentShader;
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, fraglines, fragsource, NULL);
	glCompileShader(fragmentShader);

	unsigned int shaderProgram = 0;
	int success = check_compiled_shader(vertexShader, "VERTEX");
	success &= check_compiled_shader(fragmentShader, "FRAGMENT");

	if (success) {
		shaderProgram = glCreateProgram();
		glAttachShader(shaderProgram, vertexShader);
		glAttachShader(shaderProgram, fragmentShader);
		glLinkProgram(shaderProgram);
	}

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	return shaderProgram;
}

void init_rendering()
{
	const char *vertsource[] = {
		"#version 100\n",
		"uniform mat4 uTransform;",
		"attribute vec3 aPos;",
		"attribute vec2 aTexCoord;",
		"varying vec3 vPos;",
		"varying vec2 vTexCoord;",
		"void main()",
		"{",
			"gl_Position = uTransform * vec4(aPos, 1.0);",
			"vPos = gl_Position.xyz;",
			"vTexCoord = aTexCoord;",
		"}",
	};
	const int vertlines = sizeof(vertsource)/sizeof(vertsource[0]);

	const char *fragsource[] = {
		"#version 100\n",
		"precision highp float;",
		"uniform sampler2D uTexture;",
		"uniform float uAttenuation;",
		"uniform float uAlphaTest;",
		"varying vec3 vPos;",
		"varying vec2 vTexCoord;",
		"void main()",
		"{",
			"vec4 color = texture2D(uTexture, vTexCoord);",
			"if (color.a < uAlphaTest)",
				"discard;",
			"if (uAttenuation != 0.0)",
				"color *= 1.0 - (vPos.z/uAttenuation);",
			"gl_FragColor = color;",
		"}",
	};
	const int fraglines = sizeof(fragsource)/sizeof(fragsource[0]);

	unsigned int shaderProgram = make_shader_program(vertlines, vertsource,
			fraglines, fragsource);

	glUseProgram(shaderProgram);

	uTransform = glGetUniformLocation(shaderProgram, "uTransform");

	uAttenuation = glGetUniformLocation(shaderProgram, "uAttenuation");
	glUniform1f(uAttenuation, 0);

	unsigned int uAlphaTest = glGetUniformLocation(shaderProgram, "uAlphaTest");
	glUniform1f(uAlphaTest, 0.5f);
	unsigned int uTexture = glGetUniformLocation(shaderProgram, "uTexture");
	glUniform1i(uTexture, 0);
	aPos = glGetAttribLocation(shaderProgram, "aPos");
	aTexCoord = glGetAttribLocation(shaderProgram, "aTexCoord");

	mat4x4_identity(view_matrix);
	mat4x4_ortho(projection_matrix, -1, 1, -1, 1, 127, -128);
	mat4x4_mul(view_projection_matrix, projection_matrix, view_matrix);

	const unsigned int ROW0[] = {
		0xffffffff, 0xffff00ff, 0xffff00ff, 0xffff00ff, 0xffff00ff, 0xffff00ff, 0xffff00ff, 0xffff00ff, 0xffffff00, 0xffffff00, 0xffffff00, 0xffffff00, 0xffffff00, 0xffffff00, 0xffffff00, 0xffffffff,
	};
	const unsigned int ROW1[] = {
		0xffffffff, 0xffffff00, 0xffffff00, 0xffffff00, 0xffffff00, 0xffffff00, 0xffffff00, 0xffffff00, 0xffff00ff, 0xffff00ff, 0xffff00ff, 0xffff00ff, 0xffff00ff, 0xffff00ff, 0xffff00ff, 0xffffffff,
	};

	int null_texture_row_bytes = 16*sizeof(unsigned int);
	unsigned int *null_texture_pixels = malloc(null_texture_row_bytes*16);
	memset(&null_texture_pixels[0], 0xff, null_texture_row_bytes);
	for (int i = 16; i < 128; i += 16)
		memcpy(&null_texture_pixels[i], ROW0, null_texture_row_bytes);
	for (int i = 128; i < 240; i += 16)
		memcpy(&null_texture_pixels[i], ROW1, null_texture_row_bytes);
	memset(&null_texture_pixels[240], 0xff, null_texture_row_bytes);

	glActiveTexture(GL_TEXTURE0);
	null_model.texture = new_texture(16, 16, null_texture_pixels);
	free(null_texture_pixels);

	const struct vertex NULL_VERTICES[] = {
		{-.5, -.5, 0, 0, 0},
		{+.5, -.5, 0, 1, 0},
		{-.5, +.5, 0, 0, 1},
		{+.5, +.5, 0, 1, 1},
	};

	null_model.vbo = new_vertex_buffer_object(sizeof(NULL_VERTICES),
			NULL_VERTICES);

	const unsigned short NULL_ELEMENTS[] = {
		0, 1, 2, 3, 2, 1
	};

	glGenBuffers(1, &null_model.ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, null_model.ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(NULL_ELEMENTS),
			NULL_ELEMENTS, GL_STATIC_DRAW);

	null_model.numelements = sizeof(NULL_ELEMENTS)/sizeof(NULL_ELEMENTS[0]);
}

unsigned int new_vertex_buffer_object(int numbytes, const struct vertex *data)
{
	unsigned int vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, numbytes, data, GL_STATIC_DRAW);

	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE,
			sizeof(struct vertex), (void*)(POS_X*sizeof(float)));
	glEnableVertexAttribArray(aPos);

	glVertexAttribPointer(aTexCoord, 2, GL_FLOAT, GL_FALSE,
			sizeof(struct vertex), (void*)(TEX_U*sizeof(float)));
	glEnableVertexAttribArray(aTexCoord);

	return vbo;
}

void set_model_matrix(mat4x4 m)
{
	mat4x4 transform;
	mat4x4_mul(transform, view_projection_matrix, m);
	glUniformMatrix4fv(uTransform, 1, GL_FALSE, transform[0]);
}

void set_view_matrix(mat4x4 m)
{
	mat4x4_dup(view_matrix, m);
	mat4x4_mul(view_projection_matrix, projection_matrix, view_matrix);
}

void set_projection_matrix(mat4x4 m)
{
	mat4x4_dup(projection_matrix, m);
	mat4x4_mul(view_projection_matrix, projection_matrix, view_matrix);
}

unsigned int new_texture(unsigned int width, unsigned int height, void *pixels)
{
	unsigned int texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA,
			GL_UNSIGNED_BYTE, pixels);

	// filter params are required for texturing
	// (maybe system defaults are not in ES 2.0)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	return texture;
}

void init_model(struct model *model)
{
	memcpy(model, &null_model, sizeof(struct model));
}

struct model *new_model()
{
	struct model *model = malloc(sizeof(struct model));
	init_model(model);
	return model;
}

void init_instance(struct instance *instance, struct model *model)
{
	instance->visible = 1;
	instance->model = model ? model : &null_model;
	mat4x4_identity(instance->model_matrix);
	instance->next = NULL;
}

struct instance *new_instance(struct model *model)
{
	struct instance *instance = malloc(sizeof(struct instance));
	init_instance(instance, model);
	return instance;
}

void free_instance_list(struct instance *instances)
{
	while (instances) {
		struct instance *next = instances->next;
		free(instances);
		instances = next;
	}
}

void draw_instance(struct instance *instance)
{
	if (!instance)
		return;
	if (!instance->visible)
		return;

	struct model *model = instance->model;
	glBindTexture(GL_TEXTURE_2D, model->texture);
	glBindBuffer(GL_ARRAY_BUFFER, model->vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model->ebo);

	set_model_matrix(instance->model_matrix);
	glDrawElements(GL_TRIANGLES, model->numelements, GL_UNSIGNED_SHORT, 0);
}

void draw_instance_list(struct instance *instances)
{
	while (instances) {
		draw_instance(instances);
		instances = instances->next;
	}
}
