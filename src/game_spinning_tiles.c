#include <string.h>
#include <glad/glad.h>
#include "render.h"
#include "free_camera_3d.h"

#define NUMSPRITES 4096

static unsigned int numinstances;
static struct instance *instances;
static float timer;
static float x, y, z;
static struct model model;

void init_spinning_tiles()
{
	init_model(&model);

	const struct vertex vertices[] = {
		{-.25, -.25, 0, 0, 0},
		{+.25, -.25, 0, 1, 0},
		{-.25, +.25, 0, 0, 1},
		{+.25, +.25, 0, 1, 1},
	};

	model.vbo = new_vertex_buffer_object(4*sizeof(struct vertex), vertices);

	numinstances = 0;
	instances = NULL;
	timer = 0;
	x = -7.5; y = -7.5; z = -7.5;
	init_free_camera_3d();
}

void update_spinning_tiles(float fixed_secs)
{
	timer += fixed_secs;
	while (timer >= 1.f/16) {
		struct instance *instance = new_instance(&model);

		mat4x4 model_matrix;
		mat4x4_translate(model_matrix, x, y, z);
		mat4x4_rotate_Y(model_matrix, model_matrix, (float)M_PI*timer/180);
		mat4x4_dup(instance->model_matrix, model_matrix);

		instance->next = instances;
		instances = instance;

		x += 1;
		if (x > 7.5f) {
			x = -7.5f;
			y += 1;
			if (y > 7.5f) {
				y = -7.5f;
				z += 1;
			}
		}
		++numinstances;
		timer -= 1.f/16;
	};

	update_free_camera_3d(fixed_secs);
	for (struct instance *instance = instances; instance; instance = instance->next) {
		mat4x4_rotate_Y(instance->model_matrix, instance->model_matrix,
				(float)M_PI*fixed_secs);
	}
}

void draw_spinning_tiles()
{
	draw_instance_list(instances);
}

void free_spinning_tiles()
{
	glDeleteBuffers(1, &model.vbo);
	model.vbo = 0;
	free_instance_list(instances);
}
