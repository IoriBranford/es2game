#include <stdlib.h>
#include <glad/glad.h>
#include <tmx.h>
#include <stb_image.h>
#include "render.h"
#include "render_tmx.h"

struct tmx_map_user_data {
	unsigned int maxtilesetsize;
	unsigned int *tile_ebos;
};

struct tmx_texture {
	unsigned int texture;
	int width;
	int height;
};

void* load_tmx_texture(const char *path)
{
	struct tmx_texture *texture = malloc(sizeof(struct tmx_texture));
	int channels;
	unsigned char *pixels = stbi_load(path,
			&texture->width, &texture->height, &channels, 4);
	texture->texture = new_texture(texture->width, texture->height, pixels);
	return texture;
}

void free_tmx_texture(void *p)
{
	struct tmx_texture *texture = p;
	glDeleteTextures(1, &texture->texture);
	free(texture);
}

void init_tmx_rendering()
{
	mat4x4 projection;
	mat4x4_ortho(projection, -1, 1, 1, -1, -128, 127);
	set_projection_matrix(projection);
}

void tmx_calc_vertices(struct vertex v[], int tile_xo, int tile_yo,
		int tile_w, int tile_h,
		float tile_ul_x, float tile_ul_y,
		float tileset_iw, float tileset_ih)
{
	float x0 = tile_xo;
	float x1 = tile_xo + tile_w;
	float y1 = tile_yo;
	float y0 = tile_yo - tile_h;
	float u0 = tile_ul_x/tileset_iw;
	float u1 = u0 + tile_w/tileset_iw;
	float v0 = tile_ul_y/tileset_ih;
	float v1 = v0 + tile_h/tileset_ih;

	v[0].x = x0; v[0].y = y0; v[0].z = 0; v[0].u = u0; v[0].v = v0;
	v[1].x = x0; v[1].y = y1; v[1].z = 0; v[1].u = u0; v[1].v = v1;
	v[2].x = x1; v[2].y = y0; v[2].z = 0; v[2].u = u1; v[2].v = v0;
	v[3].x = x1; v[3].y = y1; v[3].z = 0; v[3].u = u1; v[3].v = v1;
}

void tmx_tileset_calc_vertices(tmx_tileset *tileset)
{
	int xo = tileset->x_offset;
	int yo = tileset->y_offset;
	int w = tileset->tile_width;
	int h = tileset->tile_height;
	float iw = tileset->image->width;
	float ih = tileset->image->height;

	unsigned int tilecount = tileset->tilecount;
	int bytes = tilecount*sizeof(struct vertex)*4;
	struct vertex *vertices = malloc(bytes);
	struct vertex *v = vertices;
	tmx_tile *tile = tileset->tiles;

	for (int i = 0; i < tilecount; ++i) {
		float ul_x = tile->ul_x;
		float ul_y = tile->ul_y;
		tmx_calc_vertices(v, xo, yo, w, h, ul_x, ul_y, iw, ih);
		v += 4;
		++tile;
	}

	unsigned int vbo = new_vertex_buffer_object(bytes, vertices);
	free(vertices);
	tileset->user_data.integer = vbo;
}

void tmx_tileset_free_vertices(tmx_tileset *tileset)
{
	unsigned int vbo = tileset->user_data.integer;
	glDeleteBuffers(1, &vbo);
	tileset->user_data.integer = 0;
}

tmx_tile* tmx_ts_list_find_tile(tmx_tileset_list *tsl, int gid)
{
	while(tsl) {
		tmx_tileset *tileset = tsl->tileset;
		unsigned int firstgid = tsl->firstgid;
		if (firstgid + tileset->tilecount >= gid) {
			return &tileset->tiles[gid - firstgid];
		}
		tsl = tsl->next;
	}
	return NULL;
}

void draw_tmx_object(tmx_object *object, tmx_map *map)
{
	if (!object->visible)
		return;
	if (object->obj_type != OT_TILE)
		return;

	struct tmx_map_user_data *user_data = map->user_data.pointer;
	if (!user_data)
		return;

	int gid = object->content.gid;
	int flipbits = gid & (TMX_FLIPPED_HORIZONTALLY | TMX_FLIPPED_VERTICALLY);
	gid &= TMX_FLIP_BITS_REMOVAL;

	tmx_tile *tile = NULL;
	tmx_template *template = object->template_ref;
	if (template) {
		tile = tmx_ts_list_find_tile(template->tileset_ref, gid);
	} else {
		tile = map->tiles[gid];
	}

	struct tmx_texture *texture = tile->tileset->image->resource_image;
	unsigned int vbo = tile->tileset->user_data.integer;
	unsigned int ebo = user_data->tile_ebos[tile->id];

	static struct model model = {0, 0, 0, 6};
	static struct instance instance = { 1, &model, {{0}}, NULL };

	model.texture = texture->texture;
	model.vbo = vbo;
	model.ebo = ebo;

	mat4x4_translate(instance.model_matrix, object->x, object->y, 0);
	mat4x4_rotate_Z(instance.model_matrix, instance.model_matrix,
			(float)M_PI*object->rotation/180);

	if (flipbits & TMX_FLIPPED_HORIZONTALLY) {
		// TODO
	}

	if (flipbits & TMX_FLIPPED_VERTICALLY) {
		// TODO
	}

	draw_instance(&instance);
}
/*
void set_tmx_object_instance(struct instance *instance, tmx_object *object, tmx_tile **tiles)
{
	if (object->obj_type != OT_TILE)
		return;

	int gid = object->content.gid;
	int flipbits = gid & (TMX_FLIPPED_HORIZONTALLY | TMX_FLIPPED_VERTICALLY);
	gid &= TMX_FLIP_BITS_REMOVAL;

	tmx_tile *tile = NULL;
	tmx_template *template = object->template_ref;
	if (template) {
		tile = tmx_ts_list_find_tile(template->tileset_ref, gid);
	} else {
		tile = tiles[gid];
	}

	struct vertex *vertices = tile->user_data.pointer;
	struct tmx_texture *texture = tile->tileset->image->resource_image;

	instance->texture = texture->texture;

	struct vertex *sv = instance->vertices;
	memcpy(sv, vertices, VERTEX_BYTES_PER_SPRITE);

	if (flipbits & TMX_FLIPPED_HORIZONTALLY) {
		float u0 = sv[0].u;
		float u1 = sv[2].u;
		sv[0].u = u1; sv[2].u = u0;
		sv[1].u = u1; sv[3].u = u0;
	}

	if (flipbits & TMX_FLIPPED_VERTICALLY) {
		float v0 = sv[0].v;
		float v1 = sv[1].v;
		sv[0].v = v1; sv[2].v = v1;
		sv[1].v = v0; sv[3].v = v0;
	}

	mat4x4_translate(instance->model_matrix, object->x, object->y, 0);
	mat4x4_rotate_Z(instance->model_matrix, instance->model_matrix,
			(float)M_PI*object->rotation/180);

	instance->visible = object->visible;
}

struct instance* new_tmx_object_instance(tmx_object *object, tmx_tile **tiles)
{
	struct instance *instance = new_instance();
	if (object->obj_type == OT_TILE)
		set_tmx_object_instance(instance, object, tiles);
	return instance;
}

void tmx_objgr_make_instances(tmx_layer *layer, tmx_tile **tiles) {
	struct instance *instance = NULL;
	tmx_object_group *objgr = layer->content.objgr;
	for (tmx_object *object = objgr->head; object; object = object->next) {
		struct instance *newinstance = new_tmx_object_instance(object, tiles);
		newinstance->next = instance;
		instance = newinstance;
	}
	layer->user_data.pointer = instance;
}

void tmx_objgr_free_instances(tmx_layer *layer) {
	free_instance_list(layer->user_data.pointer);
	layer->user_data.pointer = NULL;
}
*/
tmx_map* new_tmx_map(const char *path)
{
	tmx_img_load_func = load_tmx_texture;
	tmx_img_free_func = free_tmx_texture;

	tmx_map *map = tmx_load(path);
	if (!map) {
		printf("%s", tmx_strerr());
		return NULL;
	}

	unsigned int maptilecount = map->tilecount;
	if (maptilecount <= 1)
		return map;

	unsigned int maxtilesetsize = 0;
	for (tmx_tileset_list *tsl = map->ts_head; tsl; tsl = tsl->next) {
		tmx_tileset_calc_vertices(tsl->tileset);
		unsigned int tilecount = tsl->tileset->tilecount;
		if (maxtilesetsize < tilecount)
			maxtilesetsize = tilecount;
	}

	unsigned int *tile_ebos = malloc(sizeof(unsigned int)*maxtilesetsize);
	glGenBuffers(maxtilesetsize, tile_ebos);
	for (int i = 0; i < maxtilesetsize; ++i) {
		unsigned short v0 = i*4;
		const unsigned short elements[] = {
			v0+0, v0+1, v0+2, v0+3, v0+2, v0+1
		};
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tile_ebos[i]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6*sizeof(unsigned short),
				elements, GL_STATIC_DRAW);
	}

	struct tmx_map_user_data *user_data = malloc(sizeof(struct tmx_map_user_data));
	user_data->maxtilesetsize = maxtilesetsize;
	user_data->tile_ebos = tile_ebos;
	map->user_data.pointer = user_data;
/*
	tmx_tile **tiles = map->tiles;
	for (tmx_layer *layer = map->ly_head; layer; layer = layer->next) {
		switch (layer->type) {
		case L_LAYER:
			int32_t *gids = layer->layer_content.gids;
			break;
		case L_OBJGR:
			tmx_objgr_make_instances(layer, map->tiles);
			break;
		}
	}
*/
	return map;
}

void draw_tmx_objgr(tmx_layer *layer, tmx_map *map)
{
	for (tmx_object *object = layer->content.objgr->head;
			object;
			object = object->next)
	{
		draw_tmx_object(object, map);
	}
}

void draw_tmx_map(tmx_map *map)
{
	for (tmx_layer *layer = map->ly_head; layer; layer = layer->next) {
		switch (layer->type) {
		case L_OBJGR:
			draw_tmx_objgr(layer, map);
			break;
		default:
			;
		}
	}
}

void free_tmx_map(tmx_map *map)
{
	struct tmx_map_user_data *user_data = map->user_data.pointer;
	if (user_data) {
		for (tmx_tileset_list *tsl = map->ts_head; tsl; tsl = tsl->next)
			tmx_tileset_free_vertices(tsl->tileset);

		glDeleteBuffers(user_data->maxtilesetsize, user_data->tile_ebos);
		free(user_data->tile_ebos);
		free(user_data);
	}

	tmx_map_free(map);
}
