#ifndef __RENDER_H__
#define __RENDER_H__

#include "linmath.h"

struct vertex {
	float x, y, z;
	float u, v;
};

struct model {
	unsigned int texture;
	unsigned int vbo, ebo;
	unsigned int numelements;
};

struct instance {
	int visible;
	struct model *model;
	mat4x4 model_matrix;
	struct instance *next;
};

#define MAX_VERTICES_PER_MODEL (1 << (8*sizeof(unsigned short)))
#define MAX_VERTEX_BYTES_PER_MODEL (MAX_VERTICES_PER_MODEL*sizeof(struct vertex))

void init_rendering();
unsigned int make_shader_program(int vertlines, const char *vertsource[],
		int fraglines, const char *fragsource[]);

void set_model_matrix(mat4x4 m);
void set_view_matrix(mat4x4 m);
void set_projection_matrix(mat4x4 m);

unsigned int new_texture(unsigned int width, unsigned int height, void *pixels);

unsigned int new_vertex_buffer_object(int numbytes, const struct vertex *data);

void init_model(struct model *model);
struct model *new_model();

void init_instance(struct instance *instance, struct model *model);
struct instance *new_instance(struct model *model);
void free_instance_list(struct instance *instances);
void draw_instance(struct instance *instance);
void draw_instance_list(struct instance *instances);

#endif
