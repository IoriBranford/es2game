<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="BKAM" tilewidth="64" tileheight="64" tilecount="4" columns="4">
 <tileoffset x="-32" y="0"/>
 <image source="BKAM.png" width="256" height="64"/>
 <tile id="0">
  <animation>
   <frame tileid="0" duration="66"/>
   <frame tileid="1" duration="66"/>
   <frame tileid="2" duration="66"/>
   <frame tileid="3" duration="66"/>
   <frame tileid="2" duration="66"/>
   <frame tileid="1" duration="66"/>
  </animation>
 </tile>
</tileset>
